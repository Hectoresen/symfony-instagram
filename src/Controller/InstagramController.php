<?php

namespace App\Controller;

use App\Form\PostFormType;
use App\Entity\Post;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class InstagramController extends AbstractController
{

    #[Route('/profile', name:"profile")]
    public function profile(EntityManagerInterface $doctrine)
    {
        $userRepo = $doctrine->getRepository(User::class);
        $user = $this->getUser();

        $userPosts= $user->getPosts();

        $userProfile = $userRepo->findBy(['username'=>$user->getUserIdentifier()]);

        $postRepo = $doctrine->getRepository((Post::class));

        return $this->render('Instagram/profile.html.twig',['userData' => $userProfile, 'userPosts'=>$userPosts] );
    }

    #[Route('/profile/newPost', name:"newPost")]
    public function newPost(Request $request, EntityManagerInterface $doctrine)
    {
        $form = $this->createForm(PostFormType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $postResult = $form->getData();
            $user = $this->getUser();
            $postResult->setUserposts($user);
            $doctrine ->persist($postResult);
            $doctrine ->flush();
            return new RedirectResponse($this->generateUrl('home'));
        }
        return $this->renderForm('Instagram/newPost.html.twig', ['postForm' =>$form]);
    }

    #[Route('/', name:"home")]
    public function posts(EntityManagerInterface $doctrine)
    {
        $repo=$doctrine->getRepository(Post::class);
        $resultPosts = $repo->findAll();
        return $this->renderForm('Instagram/home.html.twig', ['postsResults' => $resultPosts]);
    }

    #[Route('/create/user')]
    public function createUser(EntityManagerInterface $doctrine, UserPasswordHasherInterface $hasher){
        $user = new User();

        $user->setUsername('Jose');
        $user->setPassword($hasher->hashPassword($user, '123456'));

        $doctrine->persist($user);
        $doctrine->flush();
    }
}

?>